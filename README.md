# TodoList

une application de gestion de To-do List en Laravel coté BackEnd et React coté FrontEnd. L’application contient les éléments suivants :
• Système d’authentification 
• Page dédiée aux tâches à faire

# to start application without docker 

cd todolistBackend 

composer install

Creation de la base de données

php artisan migrate

create de la key encryption

php artisan passport:install


php artisan serve

application run on 127.0.0.1:8000

cd todolistFrontend && npm install && npm start

apllication start on 127.0.0.1:3000


# to start application with docker 

docker-compose up --build



